# Basebuilder 

Basebuilder is an experimental Constituent Relationship Management (CRM)
database. It is specifically designed to help you communicate information
(message) to people and organizations (community) to motivate them to do
something (e.g. take political action).

Basebuilder's primary use case is for political organizing, but can easily be
configured for any application.

Basebuilder is experimental and different from other similar programs primarily
because the database is stored on your device and optionally synchronized to a
server and/or to your fellow organizers.

## Design Goals

### Audience

Roughly speaking, CRM users can be put into three broad categories:

 * Basic Users: Don't want to spend time learning a new system, only want basic
   functionality

 * Advanced Users: Ready to spend time learning a new system, want it to work
   exactly the way they want it

 * Professionals: Have spent time learning professional programming tools,
   prioritize long term stability and maintenance of the CRM over immediate
   functionality

Basebuilder's first commitment is to basic users, and second commitment is to
professionals. That means Basebuilder tries to provide a fully functional and
easy to use database that is configured and ready to use out of the box for
most political organizing purposes.

Basebuilder also provides a robust system for customizing functionality that is
designed primarily for long term stability and maintenance at the expense of
easier point and click approaches to customizations.

### Distributed

A typical centralized application has one code base (usually available via a
web site) that is connected to one database (only accessible via the web site),
and many users who all connect to the single code base but with different user
names and passwords.

Basebuilder is a distributed application. In contrast with a centralized
application, each user typically has their own code base copied to their
computer or device. This code base maintains it's own copy of the database
(also copied to the user's computer or device) which is synchronized with the
databases of the other users. 

Although their are many options for setting up a shared Basebuilder database, a
common one:

 * Uses a central database on a server;

 * Each user has a unique login to this central database;

 * Each user's local code base is responsible for using the user's unique login
   to synchronize their data to the central database on the server;

 * Each user's local code base references the same configuration files as their
   colleagues to ensure that they are all configured the same way.

### Relationships between code, data and configuration

Like most CRM systems, Basebuilder has:

 * Data: These are your contacts and content and the interactions between them.
   It also includes your database schema (via definitions of your input and 
   display forms).

 * Configuration: This part makes your instance of Basebuilder work differently
   from other instances. It makes a development version different than a live
   version. It can futher be broken down into global configuration (shared with
   all users of your Basebuilder instance) and local (only available to your
   instance).

 * Code: This is the logic of Basebuilder. It can be further broken down into
   core code and plugin code.

All three items are related but independet of each other. The Code, for
example, makes no assumptions about your schema. The schema is completely
defined in your database. That means you can upgrade the Code anytime you want
and if something goes wrong you can downgrade it again. You can revert to any
version you want and the only impact will be that some features (and bugs) will
silently appear and disappear.

The Configuratation is designed to be completely self-contained and
transferable. You can maintain one configuration shared across all the users of
your database. 

The data includes the definition of your schema, it also defines how it should
be upgraded. All schema changes should be indempotent, but not reversible. In
other words, you can always upgrade your schema (and repeat the same upgrade
steps over and over again), but you can't downgrade it.  

Note - configuration changes and and schema upgrades are independent of code
upgrades.

## Developing

See [dev docs](docs/dev.md).
