import Immutable from 'immutable';

const Contact = Immutable.Record({
  id: '',
  text: '',
});

export default Contact;
