import Immutable from 'immutable';
import Schema from '../Schema';
import {ReduceStore} from 'flux/utils';
import ContactDispatcher from './ContactDispatcher';

class ContactListStore extends ReduceStore {
  constructor() {
    super(ContactDispatcher);
  }

  getInitialState() {
    var summaryFields = Schema.contactSummary.fields.map(function(def) {
      return def;
    });
    return Immutable.Map({
      contacts: Immutable.OrderedMap(),
      selector: Immutable.Map({type: 'contact'}),
      fields: Immutable.List(summaryFields),
      skip: 0,
      total: null,
      limit: 25
    });
  }
  reduce(state, action) {
    switch (action.type) {
      case "set_selector":
        return state.set("selector", Immutable.Map(action.data));
      case "set_skip":
        return state.set("skip", action.data);
      case "set_total":
        return state.set("total", action.data);
      case "set_limit":
        return state.set("limit", action.data);
      case "replace_contact_list":
        // Replace list
        return state.set("contacts", Immutable.OrderedMap(action.data)); 
      case "update_contact":
        if (state.hasIn(["contacts", action.data._id])) {
          return state.setIn(["contacts", action.data._id], action.data);
        }
        else {
          return state;
        }
      case "remove_contact":
        if (state.hasIn(["contacts", action.data])) {
          var total = state.get("total");
          total -= 1;
          return state.deleteIn(["contacts", action.data]).set("total", total);
        }
        return state.deleteIn(["contacts", action.data]);
      default:
        return state;
    }
  }
}

export default new ContactListStore();
