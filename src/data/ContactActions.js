import ContactDispatcher from './ContactDispatcher';
import PouchDataManager from './PouchDataManager';

/*
 * Actions taken with contacts.
 *
 * All actions are in the form: verb object.
 *
 * Most actions can be best understood as pairs. One action happens
 * against the pouchdb (an async call) that triggers another action
 * taken against the application state.
 *
 *   Affects pouchDB    Affects state
 *   --------------------------------
 *   saveContact        updateContact
 *   deleteContact      removeContact
 *   queryContacts      replaceContactList
 */
const ContactActions = {
  saveContact(contact) {
    PouchDataManager.saveContact(contact);
  },
  deleteContact(contact) {
    PouchDataManager.deleteContact(contact);
  },
  queryContacts(params = {}) {
    PouchDataManager.queryContacts(params);
  },
  saveContactAndSetContactDetail(contact) {
    PouchDataManager.saveContact(contact).then(function(res) {
      ContactActions.setContactDetail(res);
    }).catch(function(err) {
      console.log(err);
    });
  },
  getContactAndSetContactDetail(contact) {
    PouchDataManager.getContact(contact).then(function(res) {
      ContactActions.setContactDetail(res);
    }).catch(function(err) {
      console.log(err);
    });
  },
  updateContact(contact) {
    ContactDispatcher.dispatch({
      type: "update_contact",
      data: contact,
    });
  },
  removeContact(id) {
    ContactDispatcher.dispatch({
      type: "remove_contact",
      data: id,
    });
  },
  replaceContactList(contacts) {
    ContactDispatcher.dispatch({
      type: "replace_contact_list",
      data: contacts,
    });
  },
  setContactDetail(contact) {
    ContactDispatcher.dispatch({
      type: "set_contact_detail",
      data: contact,
    });
  },
  setSkip(skip) {
    ContactDispatcher.dispatch({
      type: "set_skip",
      data: skip
    });
  },
  setTotal(total) {
    ContactDispatcher.dispatch({
      type: "set_total",
      data: total
    });
  },
  setLimit(limit) {
    ContactDispatcher.dispatch({
      type: "set_limit",
      data: limit 
    });
  },
  setSelector(selector) {
    ContactDispatcher.dispatch({
      type: "set_selector",
      data: selector
    })
  }
};

export default ContactActions;
