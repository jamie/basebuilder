'use strict';

import ContactActions from './ContactActions';
import Schema from '../Schema';
import Utils from '../Utils';
import PouchDB from 'pouchdb';

// Create the database.
PouchDB.plugin(require('pouchdb-find'));
var db = new PouchDB('basebuilder');
PouchDB.debug.disable();
db.changes({
  since: 'now',
  live: true,
  include_docs: true,
}).on('change', function(change) {
  if (change.deleted) {
    // If deleted, ensure they are removed from our state.
    ContactActions.removeContact(change.id);
  }
  else {
    // If added, put them in.
    ContactActions.updateContact(change.doc);
  }
});

const PouchDataManager = {
  getContact(contact) {
    return db.get(contact._id).then(function(contact) {
      return contact;
    }).catch(function(err) {
        console.log(err);
    });
  },
  saveContact(contact) {
    console.log("Saving", contact);
    if (!contact._id) {
      contact._id = 'contact.' + Utils.uuid();
      contact.type = 'contact';
      return db.put(contact).then(function(res) {
        return db.get(res.id);
      }).catch(function(err) {
        console.log(err);
      });
    }
    else {
      return db.get(contact._id).then(function(res) {
        var save_contact = Object.assign(res, contact);
        return db.put(res);
      }).then(function(res) {
        return db.get(res.id);
      }).catch(function(err) {
        console.log(err);
      });
    }
  },
  deleteContact(id) {
    db.get(id).then(function(res) {
      return db.remove(res);
    }).catch(function(err) {
      console.log(err);
    });
  },

  queryContacts(params) {
    // Ensure we have an index on type.
    db.createIndex({
      index: {
        fields: ['type']
      }
    }).then(function(result) {
      // Now ensure we have an index on the quick search fields. 
      var quickSearchFields = Schema.quickSearch.fields.map(function(name) {
        return name;
      });
      return db.createIndex({
        index: {
          fields: quickSearchFields,
        }
      });
    }).then(function(result) {
      params.fields.push('_id');
      console.log("Querying with params", params);
      return db.find(params);
    }).then(function(result) {
      console.log("found", result.docs);
      ContactActions.replaceContactList(result.docs.reduce((acc, val) => ({ ...acc, [val._id]: val }), {}));
      if (params.refreshTotal) {
        // Now find again without params so we get a count of returned results.
        var count_params = {
          selector: params.selector,
          fields: [], 
        }
        return db.find(count_params);
      }
      else {
        return null;
      }
    }).then(function(result) {
      if (result !== null) {
        console.log("Re-ran expensive query", result.docs.length);
        ContactActions.setTotal(result.docs.length);
      }
    }).catch(function(err) {
      console.log(err);
    });
  },
};

export default PouchDataManager;
