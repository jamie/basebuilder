/**
 * Track which contact should be displayed in detail view 
 * (if any).
 **/

import Immutable from 'immutable';
import {ReduceStore} from 'flux/utils';
import ContactDispatcher from './ContactDispatcher';

class ContactDetailStore extends ReduceStore {
  constructor() {
    super(ContactDispatcher);
  }

  getInitialState() {
    // Start empty.
    return null;
  }

  reduce(state, action) {
    switch (action.type) {
      case "set_contact_detail":
        return action.data;
      case "update_contact":
        if (state && state._id == action.data._id) {
          return action.data;
        }
      default:
        return state;
    }
  }
}

export default new ContactDetailStore();
