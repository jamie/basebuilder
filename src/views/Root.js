import React from 'react';
import Contact from './contact/Contact';
import Post from './post/Post';
import Message from './message/Message';
import Setting from './setting/Setting';

class Root extends React.PureComponent {
  constructor(props) {
    super(props);
    // Areas are: post (aka Welcome), contact (aka Community, message
    // (aka Messaging) or setting (aka Setup).
    this.state = {
      area: 'post'
    }
    this.changeArea = this.changeArea.bind(this);
  }

  changeArea(area) {
    this.setState({area: area});
  }

  render() {
    var area = null;
    var postActive = '';
    var contactActive = '';
    var messageActive = '';
    var settingActive = '';
    switch(this.state.area) {
      case 'post':
        area = (
          <Post {...this.props} />
        );
        postActive = 'active';
        break;
      case 'contact':
        area = (
          <Contact {...this.props} />
        );
        contactActive = 'active';
        break;
      case 'message':
        area = (
          <Message {...this.props} />
        );
        messageActive = 'active';
        break;
      case 'setting':
        area = (
          <Setting {...this.props} />
        );
        settingActive = 'active';
        break;
      default:
        console.log("Someting went wrong. I don't know what area to send you to.");
    }
    return (
      <div>
        <ul className="nav nav-tabs">
          <li role="presentation" className={postActive}><a href="#" onClick={() => this.changeArea('post')}>Welcome</a></li>
          <li role="presentation" className={contactActive}><a onClick={() => this.changeArea('contact')} href="#">Community</a></li>
          <li role="presentation" className={messageActive}><a onClick={() => this.changeArea('message')} href="#">Messaging</a></li>
          <li role="presentation" className={settingActive}><a onClick={() => this.changeArea('setting')} href="#">Operations</a></li>
        </ul>
        {area}
      </div>
    );
  }
}

export default Root;
