/**
 * The entry point for all Contact Views.
 */

import React from 'react';

class Message extends React.Component {
  render() {
    return (
      <div>
        <h1>Messaging</h1>
        <p>See all your existing messages below or create a new message.</p>
        <p>Only create one message, and then tailor your message to go out via your web site, email, twitter, or SMS.</p>
      
      </div>
    ); 
  } 
}

export default Message;
