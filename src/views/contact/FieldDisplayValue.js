/** 
 * Display a read-only version of a given field.
 *
 * See also FieldDisplayInput which handles the editable version.
 */

import React from 'react';

class FieldDisplayValue extends React.Component {
  render() {
    var value = null;
    if (this.props.value === false) {
      // false will show up empty, which is confusing.
      value = "no";
    }
    else if (this.props.value === true) {
      value = "yes";
    }
    else if (!this.props.value || this.props.value.length == 0) {
      value = '[Double click to edit]';
    }
    else { 
      if (this.props.value.constructor === Array) {
        value = this.props.value.join(', ');
      }
      else {
        value = this.props.value;
      }
    }
    return (
      <span title="Double click to edit" onDoubleClick={this.props.startEditing}>{value}</span>
    );

  }
}

export default FieldDisplayValue;

