/*
 * Handle display of a single field (label, then delegate value). 
 *
 * FieldRow properly delegates primitive fields and list fields to the
 * appropriate view.
 *
 */

import React from 'react';
import ContactActions from '../../data/ContactActions';
import DeepFieldSummary from './DeepFieldSummary';
import DeepFieldFilter from './DeepFieldFilter';
import DeepFieldAdd from './DeepFieldAdd';
import FieldDisplay from './FieldDisplay';
import Schema from '../../Schema';

class FieldRow extends React.PureComponent{
  render() {
    var fieldName = this.props.fieldDef.name;
    if (this.props.fieldDef.deepFields) {
      var view = (
        <DeepFieldSummary 
          contact={this.props.contact}
          fieldDef={this.props.fieldDef}
          fieldName={fieldName}
        />
      );
    }
    else {
      var view = (
        <FieldDisplay
          fieldName={fieldName}
          value={this.props.contact[fieldName]}
          contact={this.props.contact}
          editable={true}
          fieldDef={this.props.fieldDef}
          rowIndex={null}
          deepField={false}
        />
      );
    }
    return (
      <tr key={this.props.fieldDef.id}>
        <td className="bb-field-label">{this.props.fieldDef.label}</td>
        <td>
          {view} 
        </td>
      </tr>
    );
  }
}

export default FieldRow;
