/** 
 * Display an editable version of a given field.
 *
 * See also FieldDisplayValue which handles the read-only version.
 */

import React from 'react';
import Creatable from 'react-select';
import Immutable from 'immutable';


class FieldDisplayInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: this.props.value }
    this.updateSelectOnChange = this.updateSelectOnChange.bind(this);
  }

  updateSelectOnChange(obj) {
    var value = null;
    if (this.props.fieldDef.type == 'list') {
      // We expect to get an indexed object of values that we need to
      // convert into a list.
      var value = Object.keys(obj).map(function(i) {
        return obj[i].value;
      });
    }
    else {
      value = obj.value;
    }
    this.setState({value: value});
    // Everytime the select field changes, we save, but we don't set
    // the editing_state to false (which is the default) because
    // then the user will have to double click again to add another element.
    // Instead, this element saves on blur.
    var editing_state = true;
    this.props.save(value, editing_state );
  }
  render() {
    switch(this.props.fieldDef.input) {
      case 'checkbox':
        return (
          <input type="checkbox" defaultChecked={this.props.value} onChange={this.props.saveCheckBoxOnChange} />
        );
      case 'select':
        // If our field type is list, then use a multi select. Otherwise, single.
        // onChange={this.props.saveSelectOnChange}
        var multi = this.props.fieldDef.type == 'list' ? true : false;
        return (
          <Creatable
            name={this.props.fieldDef.name}
            className="pop-out-input-field"
            value={this.state.value}
            options={this.props.fieldDef.options}
            onChange={this.updateSelectOnChange}
            onBlur={this.props.cancelEditing}
            autosize={true}
            multi={multi}
          />
        );
      default:
        return (
          <input autoFocus className="pop-out-input-field" onBlur={this.props.saveInputOnBlur} onKeyDown={this.props.saveInputOnEnterKey} defaultValue={this.props.value} />
        );
    }
  }
}

export default FieldDisplayInput;

