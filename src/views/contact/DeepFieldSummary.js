/*
 * Dsplay a single row of a list field with just the basic summary fields.
 *
 */

import React from 'react';
import ContactActions from '../../data/ContactActions';
import FieldDisplay from './FieldDisplay';
import Schema from '../../Schema';

class DeepFieldSummary extends React.PureComponent{
  constructor(props) {
    super(props);
    this.openList = this.openList.bind(this);
    this.closeList = this.closeList.bind(this);
    this.insertRow = this.insertRow.bind(this);
    this.deleteRow = this.deleteRow.bind(this);
    this.state = {listOpen: false};
  }

  openList() {
    this.setState({listOpen: true});
  }

  closeList() {
    this.setState({listOpen: false});
  }

  insertRow() {
    var new_row = {};
    this.props.fieldDef.deepFields.forEach(function(fieldDef) {
      new_row[fieldDef.name] = null;
    });
    var contact = this.props.contact;
    if (!contact[this.props.fieldName]) {
      contact[this.props.fieldName] = [];
    }
    contact[this.props.fieldName].push(new_row);
    ContactActions.saveContact(contact);
  }

  deleteRow(event) {
    console.log("Deleting...", event.target.value);
    // Remove one element in this array at the position specified.
    this.props.contact[this.props.fieldName].splice(event.target.value, 1);
    ContactActions.saveContact(this.props.contact);
  }

  render() {
    // If we are expanded, show every row in this field.
    if (this.state.listOpen) {
      // Gather table header fields.
      var header = this.props.fieldDef.deepFields.map(function(fieldDef) {
        return (
          <th key={fieldDef.label}>{fieldDef.label}</th>
        );
      });

      // Gather all the rows.
      var rows = null;
      if (this.props.contact[this.props.fieldName]) {
        rows = Object.keys(this.props.contact[this.props.fieldName]).map(function(index) {
          var fields = this.props.fieldDef["deepFields"].map(function(fieldDef) {
            var deepRow = this.props.contact[this.props.fieldName][index];
            return (
              <td key={index+fieldDef.name}>
                <FieldDisplay
                  fieldName={this.props.fieldName}
                  value={deepRow[fieldDef.name]}
                  contact={this.props.contact}
                  editable={true}
                  fieldDef={fieldDef}
                  rowIndex={index}
                  deepField={true}
                />
              </td>
            );
          }, this);
          return (
            <tr key={index}>
              {fields}
              <td><button onClick={this.deleteRow} className="btn btn-xs btn-danger" value={index}>Delete</button></td>
            </tr>
          );
        }, this);
      }
      return (
        <div>
          <span className="row-md-2 pull-left" onClick={this.closeList} className="glyphicon glyphicon-resize-small"></span>
          <button onClick={this.insertRow} className="btn btn-success btn-xs deep-field-add">Add</button>
          <table className="table">
            <tbody>
              <tr>{header}</tr>
              {rows}
            </tbody>
          </table>
        </div>
      );
    }
    else {
      return (
        <span onClick={this.openList} className="glyphicon glyphicon-resize-full"></span>
      );
    }
  }
}

export default DeepFieldSummary;
