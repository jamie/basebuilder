/**
 * Add handles the various ways to create new contacts in the database.
 */

import React from 'react';
import QuickAdd from './QuickAdd';
import Import from './Import';

class Add extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onChangeMethod = this.onChangeMethod.bind(this);
    this.state = {
      addMethod: 'quick'
    }
  }
  onChangeMethod(event) {
    this.setState({addMethod: event.target.value});
  }

  render() {
    var display = null;
    if (this.state.addMethod == 'quick') {
      display = (
        <QuickAdd />
      );
    }
    else if(this.state.addMethod == 'import') {
      display = (
        <Import />
      );
    }
    return (
      <div className="row">
        <div className="col-sm-3">
        <select className="form-control" defaultValue={this.state.addMethod} onChange={this.onChangeMethod}>
          <option value="quick">Quick Add</option>
          <option value="import">Import</option>
        </select>
        </div>
        <div className="col-sm-9">
        {display} 
        </div>
      </div>
    );
  }
}

export default Add;


