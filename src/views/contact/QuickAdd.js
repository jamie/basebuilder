import React from 'react';
import Utils from '../../Utils';
import Schema from '../../Schema';
import ContactActions from '../../data/ContactActions';

var ESCAPE_KEY = 27;
var ENTER_KEY = 13;

class QuickAdd extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      quickAddText: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleNewContactKeyDown = this.handleNewContactKeyDown.bind(this);
  }

  componentDidMount() {
    var setState = this.setState;
  }

  handleChange(event) {
    this.setState({quickAddText: event.target.value});
  }

  handleNewContactKeyDown(event) {
    if (event.keyCode !== ENTER_KEY) {
      return;
    }
    event.preventDefault();

    var val = this.state.quickAddText.trim();

    if (val) {
      try {
        var contact = Utils.parseStringToContact(val);
      }
      catch(err) {
        console.log(err.message);
      }
      if (contact) {
        ContactActions.saveContactAndSetContactDetail(contact);
        this.setState({quickAddText: ''});
      }
    }
  }

  render() {
    var placeholder = Schema.quickAdd.instructions;
    return (
      <div>
          <input
            className="new-contact form-control"
            placeholder={placeholder}
            value={this.state.quickAddText}
            onKeyDown={this.handleNewContactKeyDown}
            onChange={this.handleChange}
            autoFocus={true}
          />
      </div>
    );
  }
}

export default QuickAdd;

