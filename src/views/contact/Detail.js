/*
 * Detail displays all fields for a given contact.
 */

import React from 'react';
import Schema from '../../Schema';
import FieldRow from './FieldRow';
import LightBox from '../LightBox';
import ContactActions from '../../data/ContactActions';

class Detail extends React.PureComponent{
  constructor(props) {
    super(props);
    this.deleteContact = this.deleteContact.bind(this);
    this.closeLightBox = this.closeLightBox.bind(this);
  }

  closeLightBox() {
    ContactActions.setContactDetail(null);
  }

  deleteContact() {
    ContactActions.deleteContact(this.props.contact._id);
    this.closeLightBox();
  }

  // contactDetailView is a table with a row for each field in the database
  render() {
    // Collect all fields for this contact in the contactDetail variable.
    var contactDetail = Schema.contactDetail.fields.map(function(field_name) {
      var fieldDef = Schema.fields[field_name];
      return (
        <FieldRow
          key={fieldDef.name}
          contact={this.props.contact}
          fieldDef={fieldDef}
        />
      );
    }, this);

    // Display the contactDetail in a light box.
    return (
      <LightBox
        deleteContact={this.deleteContact}
        closeLightBox={this.closeLightBox}
        content={contactDetail}
      /> 
    );
  }
}

export default Detail;
