/** 
 * Display a single field value for a deep field.
 *
 * See also SimpleFieldDisplay which handles simple fields.
 *
 * This view handles toggling between display a read-only version of the field
 * value or a editable version (delegating to FieldDisplayValue and
 * FieldDisplayInput).  However, we handle saving the field.
 */

import React from 'react';
import Utils from '../../Utils';
import Schema from '../../Schema';
import ContactActions from '../../data/ContactActions';
import FieldDisplayValue from './FieldDisplayValue';
import FieldDisplayInput from './FieldDisplayInput';

class FieldDisplay extends React.PureComponent{
  constructor(props) {
    super(props);
    /**
     * The state variables are:
     * editing: whether or not we are in the process of editing the field.
     *   If true, show a form input field that allows modifications, if
     *   false, just display the value.
     * original_value: the value of the field when we started editing.
     *   We keep track of this value so we know if the value changed
     *   while we were editing to avoid conflicts.
     */
    this.state = { editing: false, original_value: this.props.value };
    this.startEditing = this.startEditing.bind(this);
    this.cancelEditing = this.cancelEditing.bind(this);
    this.saveInputOnEnterKey = this.saveInputOnEnterKey.bind(this);
    this.saveInputOnBlur = this.saveInputOnBlur.bind(this);
    this.saveCheckBoxOnChange = this.saveCheckBoxOnChange.bind(this);
    this.save = this.save.bind(this);
  }

  cancelEditing() {
    this.setState({ editing: false });
  }

  saveInputOnEnterKey(event) {
    if (event.key == 'Enter' ) {
      this.save(event.target.value);
    }
  }

  saveInputOnBlur(event) {
    this.save(event.target.value);
  }

  saveCheckBoxOnChange(event) {
    var value;
    if (event.target.checked) {
        value = true; 
    }
    else {
      value = false;
    }
    this.save(value);
  }

  save(value, editing_state = false) {
    if (value === this.state.original_value) {
      // Same value that we started with, don't save.
      console.log("Not saving, same value.");
      this.setState({ editing: editing_state });
      return;
    }
    else if (this.state.original_value != this.props.value) {
      var valueHasChanged;
      if (this.props.value.constructor !== Array) {
        // If we are not an array, then the first comparison is enough.
        // The value has changed, we shouldn't save.
        valueHasChanged = true;
      }
      else {
        // If we are an array, we need to do a shallow comparison to be
        // sure.
        var valueHasChanged = false;
        for(var key in this.props.value) {
          if (this.props.value[key] !== this.state.original_value[key]) {
            valueHasChanged = true;
          }
        }
      }
      if (valueHasChanged) {
        // It's been changed out from under us! Warn the user.
        console.log("Field has been changed while we were editing it. Not saved.", this.state.original_value, this.props.value);
        return;
      }
    }

    this.setState({editing: editing_state});
    this.setState({original_value: value});
    this.updateContactField(value);
  }

  startEditing() {
    // Keep track of the value (will be used to display to the user) as
    // well as the original state so we can alert the user if it gets 
    // changed out from under us (to avoid a save conflict).
    this.setState({ 
      original_value: this.props.value,
      editing: true
    });
  }

  updateContactField(value) {
    var fieldName = this.props.fieldName;
    if (this.props.deepField) {
      // This is a deep field.
      var deepFieldName = this.props.fieldDef["name"];
      var rowIndex = this.props.rowIndex;
      if (rowIndex == null) {
        // This is a new row. Create a new record with just this value.
        var row = Schema.fields[fieldName]['addTemplate'];
        row[deepFieldName] = value;
        if (!this.props.contact[fieldName]) {
          // This field has never been set with any value.
          this.props.contact[fieldName] = [];
        }
        this.props.contact[fieldName].push(row);
      }
      else {
        // This row exists, just change one deep field value.
        this.props.contact[fieldName][rowIndex][deepFieldName] = value;
      }
    }
    else {
      // This is a simple field.
      this.props.contact[fieldName] = value;
    }
    ContactActions.saveContact(this.props.contact);
  }

  render() {
    var editing = null;
    var readonly = null;
    if (this.state.editing || this.props.fieldDef.type == 'boolean') {
      // Form input display.
       editing = (
        <FieldDisplayInput
          fieldDef={this.props.fieldDef}
          value={this.props.value}
          saveCheckBoxOnChange={this.saveCheckBoxOnChange}
          save={this.save}
          saveInputOnBlur={this.saveInputOnBlur}
          saveInputOnEnterKey={this.saveInputOnEnterKey}
          cancelEditing={this.cancelEditing}
        />
      );
    }
    if (this.props.fieldDef.type != 'boolean') {
      readonly = (
        <FieldDisplayValue
          fieldDef={this.props.fieldDef}
          startEditing={this.startEditing}
          value={this.props.value}
          editable={this.props.editable}
        />
      ); 
    }
    return (
      <div>
        {editing}
        {readonly}
      </div>
    )
  }
}

export default FieldDisplay;
