import React from 'react';
import Utils from '../../Utils';
import Schema from '../../Schema';
import ContactActions from '../../data/ContactActions';

var ESCAPE_KEY = 27;
var ENTER_KEY = 13;

class QuickSearch extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      quickSearchText: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSearchContactKeyDown = this.handleSearchContactKeyDown.bind(this);
  }

  componentDidMount() {
    var setState = this.setState;
  }

  handleChange(event) {
    this.setState({quickSearchText: event.target.value});
  }

  handleSearchContactKeyDown(event) {
    if (event.keyCode !== ENTER_KEY) {
      return;
    }
    event.preventDefault();

    var val = this.state.quickSearchText.trim();

    if (val) {
      var selector = Utils.createQuickSearchSelector(val);
      ContactActions.setSelector(selector);
    }
  }

  render() {
    var placeholder = Schema.quickSearch.instructions;
    return (
      <div>
          <input
            className="search-contact form-control"
            placeholder={placeholder}
            value={this.state.quickSearchText}
            onKeyDown={this.handleSearchContactKeyDown}
            onChange={this.handleChange}
            autoFocus={true}
          />
      </div>
    );
  }
}

export default QuickSearch;

