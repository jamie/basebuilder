/*
 * Display single row for a given contact with just basic summary fields.
 */

import Detail from './Detail';
import FieldDisplay from './FieldDisplay';
import React from 'react';
import Schema from '../../Schema';
import ContactActions from '../../data/ContactActions';

class Summary extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { detailDisplay: false };
    this.deleteContact = this.deleteContact.bind(this);
    this.openDetail = this.openDetail.bind(this);
  }

  openDetail() {
    ContactActions.getContactAndSetContactDetail(this.props.contact)
  }

  deleteContact() {
    ContactActions.deleteContact(this.props.contact._id);
  }

  render() {
    // Build component that shows each field that is defined as a summaryField,
    // all in a single row. 
    //
    // Iterate over each defined Summary Field.
    var contactSummary = Schema.contactSummary.fields.map(function(fieldName) {
      var fieldDef = Schema.fields[fieldName];

      // Differentiate between "deep" fields - these are fields that can
      // hold rows of data within them...
      if (fieldDef.type == "deep") {
        // This field contains a list of rows, but we can only display one
        // field. We need to determine which row from this list to pick AND 
        // which field in the chosen row to display.

        // Gather the configuration data we'll use to figure this out.
        var testProperty = Schema.fields[fieldName]["summaryTestProperty"];
        var testValue = Schema.fields[fieldName]["summaryTestValue"];
        var displayField = Schema.fields[fieldName]["summaryDisplayField"];

        // Iterate over every row in this field looking for a property
        // that matches the value we are seeking. Return the first one
        // found.
        var rowIndex = null;
        if (this.props.contact[fieldName]) {
          Object.keys(this.props.contact[fieldName]).forEach(function(index) {
            var row = this.props.contact[fieldName][index];
            if (row[testProperty] == testValue) {
              rowIndex = index;
              return;
            }
          }, this);  
        }

        // Retrieve the value of this field, if any.
        var value = null;
        if (rowIndex) {
          value = this.props.contact[fieldName][rowIndex][displayField];
        }

        // Since we are in summary mode, and this is a deep field, we don't
        // allow the field to be edited if there is no value - you can only
        // add a deep field from the DeepFieldDetail view (that way you are
        // sure to enter all the fields for the row, not just the one in the
        // summary).
        var editable = false;
        if (value) {
          editable = true;
        }

        // Retrieve the field definition for the deep field we are displaying.
        var deepFieldDef = Schema.fields[fieldName]['deepFields'].find(function(field) {
          return field.name === displayField;
        });
        return (
          <td key={fieldName}>
            <FieldDisplay
              fieldName={fieldName}
              value={value}
              contact={this.props.contact}
              editable={editable}
              fieldDef={deepFieldDef}
              rowIndex={rowIndex}
              deepField={true}
            /> 
          </td>
        );
      }
      else {
        // ... and simple fields.
        var value = this.props.contact[fieldName];
        return (
          <td key={fieldName}>
            <FieldDisplay
              fieldName={fieldName}
              value={value}
              contact={this.props.contact}
              editable={true}
              fieldDef={fieldDef}
              rowIndex={null}
              deepField={false}
            />
          </td>
        );
      }
    }, this);
    
    return (
      <tr>
        {contactSummary}
        <td>
          <button onClick={this.openDetail} className="btn btn-info">Open</button>
          <button onClick={this.deleteContact} className="btn btn-danger">Delete</button>
        </td>
      </tr>
    );
  }
}

export default Summary;
