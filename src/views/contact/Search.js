import React from 'react';
import QuickSearch from './QuickSearch';

class Search extends React.PureComponent {
  constructor(props) {
    super(props);
    this.onChangeMethod = this.onChangeMethod.bind(this);
    this.state = {
      searchMethod: 'quick'
    }
  }
  onChangeMethod(event) {
    this.setState({searchMethod: event.target.value});
  }

  render() {
    var display = null;
    if (this.state.searchMethod == 'quick') {
      display = (
        <QuickSearch />
      );
    }
    return (
      <div className="row">
        <div className="col-sm-3">
        <select className="form-control" defaultValue={this.state.searchMethod} onChange={this.onChangeMethod}>
          <option value="quick">Quick Search</option>
        </select>
        </div>
        <div className="col-sm-9">
        {display} 
        </div>
      </div>
    );
  }
}

export default Search;


