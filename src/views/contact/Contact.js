/**
 * The entry point for all Contact Views.
 */

import Schema from '../../Schema';
import Add from './Add';
import Search from './Search';
import Summary from './Summary';
import Detail from './Detail';
import PagerView from '../PagerView';
import ContactActions from '../../data/ContactActions';
import React from 'react';
import Immutable from 'immutable';

class Contact extends React.Component {
  constructor(props) {
    super(props);
    this.onChangePage = this.onChangePage.bind(this);
    this.onChangeLimit = this.onChangeLimit.bind(this);
    this.goToPreviousPage = this.goToPreviousPage.bind(this);
    this.goToNextPage = this.goToNextPage.bind(this);

    // Initial state. All Contacts.
    this.state = {
      page: 1
    };
  }

  componentWillMount() {
    // Create the table header. This just needs to be rendered once. 
    this.contactSummaryHeader = Schema.contactSummary.fields.map(function(field_name) {
      var field = Schema.fields[field_name];
      return (
        <th key={field.name}>{field.label}</th>
      );
    });
    // this.queryContacts();
  }

  queryContacts(limit = null, skip = null, selector = null) {
    var refreshTotal = false;
    if (limit === null) {
      limit = this.props.contactList.get("limit");
    }
    if (skip === null) {
      skip = this.props.contactList.get("skip");
    }
    if (selector === null) {
      selector = this.props.contactList.get("selector").toObject();
    }
    else {
      refreshTotal = true;
    }
    var params = {
      selector: selector,
      fields: this.props.contactList.get("fields").toArray(),
      limit: parseInt(limit), 
      skip: parseInt(skip),
    }
    var total = this.props.contactList.get("total");
    if (total === null) {
      refreshTotal = true
    }
    params.refreshTotal = refreshTotal;
    ContactActions.queryContacts(params);
  }

  componentWillReceiveProps(nextProps) {
    // Any time limit or skip changes, we have to requery.
    var nextField = null;
    var curField = null;
    var monitorFields = [ "limit", "skip", "selector" ];
    monitorFields.forEach(function(field) {
      nextField = nextProps.contactList.get(field);
      curField = this.props.contactList.get(field);
      if (nextField != curField) {
        if (field == "selector" && nextProps.contactList.get("skip") != 0) {
          // Reset skip (which will retrigger a new query when the new skip prop
          // comes through). If we don't, the new query will end up on whatever 
          // page we were on before - which might no longer have any records to display.
          ContactActions.setSkip(0);
          this.setState({page: 1});
          return;
        }
        this.queryContacts(nextProps.contactList.get("limit"), nextProps.contactList.get("skip"), nextProps.contactList.get("selector").toObject());
        return;
      }
    }, this);
  }

  onChangeLimit(event) {
    ContactActions.setLimit(event.target.value);
  }

  goToPreviousPage() {
    var newPage = parseInt(this.state.page) - 1;
    newPage = this.checkPage(newPage); 
    this.setSkipForPage(newPage);
    this.setState({page: newPage});
  }

  goToNextPage() {
    var newPage = parseInt(this.state.page) + 1;
    newPage = this.checkPage(newPage); 
    this.setSkipForPage(newPage);
    this.setState({page: newPage});
  }

  /**
   * Make sure our page selection doesn't exceed our limits.
   **/
  checkPage(page) {
    page = parseInt(page);
    if (page < 1) {
      return 1;
    }
    var lastPage = Math.ceil(this.props.contactList.get("total") / this.props.contactList.get("limit"));
    if (page > lastPage) {
      return lastPage;
    }
    return page;
  }

  onChangePage(event) {
    // When entering a new value for page, the user will always backspace
    // over the existing value. That shouldn't trigger a requery. Wait til
    // we get a value.
    if (!event.target.value) {
      this.setState({page: ''});
      return;
    }
    var newPage = this.checkPage(event.target.value); 
    this.setSkipForPage(newPage); 
    this.setState({page: newPage});
  }

  setSkipForPage(page) {
    var skip = (page - 1) * this.props.contactList.get("limit");
    if (isNaN(skip)) {
      // If something goes wrong, don't create a loop.
      return;
    }
    if (skip < 0) {
      skip = 0;
    }
    ContactActions.setSkip(skip);
  }

  render() {
    var contactSummaries = this.props.contactList.get("contacts").valueSeq().map(function(contact) {
        return (
          <Summary
            key={contact._id}
            contact={contact}
          />
        );
    }, this);

    var detailDisplay = null;
    if (this.props.contactDetail != null) {
      detailDisplay = (
        <Detail
          contact={this.props.contactDetail}
        />
      );
    }

    var contactListDisplay = null;
    if (this.props.contactList.get("total") !== null) {
      contactListDisplay = (
        <div>
          <PagerView
            goToNextPage={this.goToNextPage}
            goToPreviousPage={this.goToPreviousPage}
            page={this.state.page}
            limit={this.props.contactList.get("limit")}
            onChangePage={this.onChangePage}
            onChangeLimit={this.onChangeLimit}
            totalPages={Math.ceil(this.props.contactList.get("total") / this.props.contactList.get("limit"))}
            totalRecords={this.props.contactList.get("total")}
          />
          <table className="table table-striped">
            <tbody>
              <tr>
                {this.contactSummaryHeader}
                <td>&nbsp;</td>
              </tr>
              {contactSummaries}
            </tbody>
          </table>
        </div>
      );
    }
    return (
      <div>
        <header className="header">
          <h1>Contacts</h1>
          <Add />
          <Search />
        </header>
        {detailDisplay}
        {contactListDisplay} 
      </div>
    );
  }
}

export default Contact;
