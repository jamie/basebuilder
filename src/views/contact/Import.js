import React from 'react';
import Papa from 'papaparse';
import ContactActions from '../../data/ContactActions';

class Import extends React.PureComponent {
  processFile(event) {
    var files = event.target.files;
    console.log("Processing");
    for (var i = 0, f; f = files[i]; i++) {
      console.log("Parsing", f);
      var t0 = performance.now();
			Papa.parse(f, {
        header: true,
			  complete: function(results) {
          results.data.forEach(function(data) {
            console.log("Saving", data);
            ContactActions.saveContact(data);
          });
          var t1 = performance.now();
          console.log("Call took " + ((t1 - t0) * 1000) + " seconds.")
        },
        error: function(err, file) {
          console.log("Error", err, file);
        }

			});
 
    }
  }
  render() {
    console.log("rendering");
    return (
      <div>
        <input onChange={this.processFile} className="form-control"  type="file" id="files" name="files[]"  />    
      </div>
    );
  }
}

export default Import;


