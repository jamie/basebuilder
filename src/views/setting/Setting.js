/**
 * The entry point for all Contact Views.
 */

import React from 'react';

class Setting extends React.Component {
  render() {
    return (
      <div>
        <h1>Setup</h1>
        <p>Create new fields and field sets here. Review your logs.</p>
      
      </div>
    ); 
  } 
}

export default Setting;
