import React from 'react';

class LightBox extends React.PureComponent{
  constructor(props) {
    super(props);
    
    this.blackOverlayStyles = {
        background: 'black',
        opacity: '.5',
        position: 'fixed',
        top: '0px',
        bottom: '0px',
        left: '0px',
        right: '0px',
        zIndex: '100'
    }

  }
  render() {
    // console.log(this.props);
    var display_block = { "display": "block" };
    var no_border_buttom = { "borderBottom": "none" };
    return (
      <div>
        <div style={this.blackOverlayStyles} />
        <div className="modal model-open" style={display_block} role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header row" style={no_border_buttom}>
                <div className="col-sm-4">
                  <button onClick={this.props.deleteContact} className="btn btn-danger">delete</button>
                </div>
                <div className="col-sm-4">
                  <div className="bb-explanation">Double click field to edit</div>
                </div>
                <div className="col-sm-4">
                  <button onClick={this.props.closeLightBox} className="btn btn-success">save and close</button>
                </div>
              </div>
              <div className="modal-body">
                <table className="table">
                  <tbody>
                    {this.props.content}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default LightBox;
