/**
 * The entry point for all Contact Views.
 */

import React from 'react';

class Post extends React.Component {
  render() {
    return (
      <div>
        <h1>Bulletin Board</h1>
        <p>Welcome to your database! Check out the bulletin board below or post
        your own messages.</p>
      
      </div>
    ); 
  } 
}

export default Post;
