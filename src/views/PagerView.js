import React from 'react';

class PagerView extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    var totalRecords = "Calculating...";
    var totalPages = "Calculating...";
    if (this.props.totalRecords || this.props.totalRecords == 0) {
      totalRecords = this.props.totalRecords;
      totalPages = this.props.totalPages;
    }
    return (
      <div className="row pagination">
        <div className="col-sm-3 text-center">
          <button className="btn btn-default btn-sm" type="button" onClick={this.props.goToPreviousPage} aria-label="Backward"><span className="glyphicon glyphicon-backward"></span></button>
          <button className="btn btn-default btn-sm" type="button" onClick={this.props.goToNextPage} aria-label="Forward"><span className="glyphicon glyphicon-forward"></span></button>
        </div>
        <div className="col-sm-3 text-center">
          <div className="input-group input-group-sm">
            <span className="input-group-addon" id="page-addon">Page</span>
            <input type="text" className="form-control" onChange={this.props.onChangePage} value={this.props.page} aria-describedby="page-addon"/>
            <span className="input-group-addon">of {totalPages}</span>
          </div>
        </div>
        <div className="col-sm-3 text-center">
          <div className="input-group input-group-sm">
            <input className="form-control" onChange={this.props.onChangeLimit} id="limit" name="limit" value={this.props.limit}/>
            <span className="input-group-addon">per page</span>
          </div>
        </div>
        <div className="col-sm-3 total-records text-center">
          <span className="total-records-number">{totalRecords}</span> <span className="total-records-label">total records found</span>
        </div>
      </div>
    );
  }
}

export default PagerView;


