import React from 'react';
import Root from './Root';

function AppView(props) {
  return (
    <div>
      <Root {...props} />
    </div>
  );
}

export default AppView;
