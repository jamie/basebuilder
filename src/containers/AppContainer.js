import AppView from '../views/AppView';
import {Container} from 'flux/utils';
import ContactListStore from '../data/ContactListStore';
import ContactDetailStore from '../data/ContactDetailStore';
import ContactActions from '../data/ContactActions';
import PouchDataManager from '../data/PouchDataManager';
import Immutable from 'immutable';


function getStores() {
  return [
    ContactListStore,
    ContactDetailStore,
  ];
}

function getState() {
  return {
   contactList: ContactListStore.getState(),
   contactDetail: ContactDetailStore.getState(),
  }
}

export default Container.createFunctional(AppView, getStores, getState);
