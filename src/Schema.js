const Schema = {
  // Fetch the configuration json file
  // FIXME should be dynamically pulled in not set like this.
  fields: {
    'first_name': {
      name: 'first_name',
      label: 'First Name',
      type: 'string',
      quickSearchSelector: '$regex',
      quickAddRegex: /[ \u00c0-\u01ffa-zA-Z'\-]/, 
      input: 'text'
    },
    'last_name': {
      name: 'last_name',
      label: 'Last Name',
      type: 'string',
      quickSearchSelector: '$regex',
      quickAddRegex: /[ \u00c0-\u01ffa-zA-Z'\-]/, 
      input: 'text'
    },
    'email': {
      name: 'email',
      label: 'Email',
      type: 'deep',
      quickSearchSelector: '$regex',
      quickAddRegex: /^\S+@\S+$/,
      quickAddField: 'address',
      summaryTestProperty: 'is_primary',
      summaryTestValue: true,
      summaryDisplayField: 'address',
      addTemplate: {
        is_primary: true,
        type: 'personal'
      },
      deepFields: [
        { name: 'is_primary',  label: 'Is Primary', input: 'checkbox', type: 'boolean' },
        { name: 'address', label: 'Address', input: 'text', type: 'string' },
        { name: 'type', label: 'Type', input: 'select', type: 'string', options: [
            { value: 'personal', label: 'Personal' },
            { value: 'work', label: 'Work' },
          ]
        }
      ]
    },
    'phone': {
      name: 'phone',
      label: 'Phone',
      type: 'deep',
      quickSearchSelector: '$regex',
      quickAddRegex: /\+1([0-9]{10})/,
      quickAddField: 'number',
      summaryTestProperty: 'is_primary',
      summaryTestValue: true,
      summaryDisplayField: 'number',
      addTemplate: {
        is_primary: true,
        type: 'mobile'
      },
      deepFields: [
        { name: 'is_primary',  label: 'Is Primary', input: 'checkbox', type: 'boolean' },
        { name: 'number', label: 'Number', input: 'text', type: 'string' },
        { name: 'type', label: 'Type', input: 'select', type: 'string', options: 
          [
            { value: 'mobile', label: 'Mobile' },
            { value: 'home', label: 'Home' },
            { value: 'work', label: 'Work' },
          ]
        }
      ]
    },
    'address': {
      name: 'address',
      label: 'Address',
      type: 'deep',
      deepFields: [
        { name: 'is_primary',  label: 'Is Primary', input: 'checkbox', type: 'boolean' },
        { name: 'street_address', label: 'Street Address', input: 'text', type: 'string' },
        { name: 'city', label: 'City', input: 'text', type: 'string' },
        { name: 'state', label: 'State', input: 'select', type: 'string', options: 
          [
            { value: 'NY', label: 'New York' },
            { value: 'CA', label: 'California' } 
          ]
        },
        { name: 'postal_code', label: 'Postal Code', input: 'text', type: 'string' },
        { name: 'type', label: 'Type', input: 'select', type: 'string', options: 
          [
            { value: 'home', label: 'Home' },
            { value: 'work', label: 'Work' }
          ]
        }
      ]
    },
    'tags': {
      name: 'tags',
      label: 'Tags',
      type: 'list',
      input: 'select',
      options: [ 
        { value: 'staff', label: 'Staff' },
        { value: 'board', label: 'Board' }
      ]
    }
  },
  quickSearch: {
    instructions: 'Search first name, last name, email or phone number',
    fields: [ 'first_name', 'last_name', 'email', 'phone' ]
  },
  quickAdd: {
    instructions: 'Add new contact by typing: Firstname Lastname +1.505.555.1212 user@domain.org',
    fields: [ 'phone', 'email', 'first_name', 'last_name' ]
  },
  contactSummary: {
    fields: [ 'first_name', 'last_name', 'email', 'phone' ]
  },
  contactDetail: {
    fields: [ 'first_name', 'last_name', 'email', 'phone', 'address', 'tags' ]
  }
};

export default Schema; 
