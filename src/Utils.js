import Schema from './Schema';
import React from 'react';

const Utils = {
  uuid() {
    /*jshint bitwise:false */
    var i, random;
    var uuid = '';

    for (i = 0; i < 32; i++) {
      random = Math.random() * 16 | 0;
      if (i === 8 || i === 12 || i === 16 || i === 20) {
        uuid += '-';
      }
      uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random))
        .toString(16);
    }

    return uuid;
  },

  createQuickSearchSelector(val) {
    var selector = { $or: []};
    Schema.quickSearch.fields.forEach(function(fieldName) {
      var field = Schema.fields[fieldName];
      var field_def = {};
      field_def[fieldName] = {};
      field_def[fieldName][field.quickSearchSelector] = val;
      selector.$or.push(field_def);
    });
    return selector;
  },

  pluralize(count, word) {
    return count === 1 ? word : word + 's';
  },

  /**
   * Convert a quick add string to a proper contact object ready to be saved.
   **/
  parseStringToContact(str) {
    var pieces = str.trim().split(/[ ]+/);
    var fields = Schema.quickAdd.fields;
    var contact = {}
    var foundOneField = false;
    // Iterate over each space separated word give by the user.
    pieces.forEach(function(piece) {
      // Iterate over every quick add field.
      fields.forEach(function (fieldName) {
        var field = Schema.fields[fieldName];
        // If we have used a piece, it will be reset to null and skipped,
        // otherwise, it's in play so let's test this piece against each
        // field.
        if (piece && !contact[field.name] && field.quickAddRegex.test(piece)) {
          // We have a match.
          if (field.deepFields) {
            // This is a deep field, not a primitive field.
            contact[field.name] = [];
            // Grab the template and insert the value.
            var new_value = field.addTemplate;
            new_value[field.quickAddField] = piece;
            contact[field.name].push(new_value);
          }
          else {
            contact[field.name] = piece;
          }
          foundOneField = true;
          piece = null;
        }
      });
    });

    if (foundOneField) {
      console.log("boo", contact);
      return contact;
    }
    console.log("Throwing error");
    throw new SyntaxError("I tried to convert what you typed into fields but failed. Please use the following format: " + instructions);
  },
};

export default Utils;
