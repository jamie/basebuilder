# Developing

To get started, download dependencies with: 

    npm install

Then, build the bundle.js file with:

    npm run watch

Lastly, launch electron to view the app:

  npm start
